﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Pharmecy1Web.Models;

namespace Pharmecy1Web.Controllers
{
    public class Capsule_Encapsulation_CategoryController : Controller
    {
        private PharmecyEntities db = new PharmecyEntities();

        // GET: Capsule_Encapsulation_Category
        public async Task<ActionResult> Index()
        {
            var capsule_Encapsulation_Category = db.Capsule_Encapsulation_Category.Include(c => c.Category);
            return View(await capsule_Encapsulation_Category.ToListAsync());
        }

        // GET: Capsule_Encapsulation_Category/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Capsule_Encapsulation_Category capsule_Encapsulation_Category = await db.Capsule_Encapsulation_Category.FindAsync(id);
            if (capsule_Encapsulation_Category == null)
            {
                return HttpNotFound();
            }
            return View(capsule_Encapsulation_Category);
        }

        // GET: Capsule_Encapsulation_Category/Create
        public ActionResult Create()
        {
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category");
            return View();
        }

        // POST: Capsule_Encapsulation_Category/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id_capsule,Name,Output,Capsule_Size,Machine_Dimension,Hipping_Weight,Id_Category,Avatar")] Capsule_Encapsulation_Category capsule_Encapsulation_Category)
        {
            if (ModelState.IsValid)
            {
                db.Capsule_Encapsulation_Category.Add(capsule_Encapsulation_Category);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", capsule_Encapsulation_Category.Id_Category);
            return View(capsule_Encapsulation_Category);
        }

        // GET: Capsule_Encapsulation_Category/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Capsule_Encapsulation_Category capsule_Encapsulation_Category = await db.Capsule_Encapsulation_Category.FindAsync(id);
            if (capsule_Encapsulation_Category == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", capsule_Encapsulation_Category.Id_Category);
            return View(capsule_Encapsulation_Category);
        }

        // POST: Capsule_Encapsulation_Category/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id_capsule,Name,Output,Capsule_Size,Machine_Dimension,Hipping_Weight,Id_Category,Avatar")] Capsule_Encapsulation_Category capsule_Encapsulation_Category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(capsule_Encapsulation_Category).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", capsule_Encapsulation_Category.Id_Category);
            return View(capsule_Encapsulation_Category);
        }

        // GET: Capsule_Encapsulation_Category/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Capsule_Encapsulation_Category capsule_Encapsulation_Category = await db.Capsule_Encapsulation_Category.FindAsync(id);
            if (capsule_Encapsulation_Category == null)
            {
                return HttpNotFound();
            }
            return View(capsule_Encapsulation_Category);
        }

        // POST: Capsule_Encapsulation_Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            Capsule_Encapsulation_Category capsule_Encapsulation_Category = await db.Capsule_Encapsulation_Category.FindAsync(id);
            db.Capsule_Encapsulation_Category.Remove(capsule_Encapsulation_Category);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
