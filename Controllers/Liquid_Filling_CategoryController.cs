﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Pharmecy1Web.Models;

namespace Pharmecy1Web.Controllers
{
    public class Liquid_Filling_CategoryController : Controller
    {
        private PharmecyEntities db = new PharmecyEntities();

        // GET: Liquid_Filling_Category
        public async Task<ActionResult> Index()
        {
            var liquid_Filling_Category = db.Liquid_Filling_Category.Include(l => l.Category);
            return View(await liquid_Filling_Category.ToListAsync());
        }

        // GET: Liquid_Filling_Category/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Liquid_Filling_Category liquid_Filling_Category = await db.Liquid_Filling_Category.FindAsync(id);
            if (liquid_Filling_Category == null)
            {
                return HttpNotFound();
            }
            return View(liquid_Filling_Category);
        }

        // GET: Liquid_Filling_Category/Create
        public ActionResult Create()
        {
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category");
            return View();
        }

        // POST: Liquid_Filling_Category/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id_Liquid_Filling,Name,Air_Pressure,Air_Volumn,Filling_Speed,Filling_Range,Id_Category,Avatar")] Liquid_Filling_Category liquid_Filling_Category)
        {
            if (ModelState.IsValid)
            {
                db.Liquid_Filling_Category.Add(liquid_Filling_Category);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", liquid_Filling_Category.Id_Category);
            return View(liquid_Filling_Category);
        }

        // GET: Liquid_Filling_Category/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Liquid_Filling_Category liquid_Filling_Category = await db.Liquid_Filling_Category.FindAsync(id);
            if (liquid_Filling_Category == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", liquid_Filling_Category.Id_Category);
            return View(liquid_Filling_Category);
        }

        // POST: Liquid_Filling_Category/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id_Liquid_Filling,Name,Air_Pressure,Air_Volumn,Filling_Speed,Filling_Range,Id_Category,Avatar")] Liquid_Filling_Category liquid_Filling_Category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(liquid_Filling_Category).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Id_Category = new SelectList(db.Categories, "Id_Category", "Name_Category", liquid_Filling_Category.Id_Category);
            return View(liquid_Filling_Category);
        }

        // GET: Liquid_Filling_Category/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Liquid_Filling_Category liquid_Filling_Category = await db.Liquid_Filling_Category.FindAsync(id);
            if (liquid_Filling_Category == null)
            {
                return HttpNotFound();
            }
            return View(liquid_Filling_Category);
        }

        // POST: Liquid_Filling_Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            Liquid_Filling_Category liquid_Filling_Category = await db.Liquid_Filling_Category.FindAsync(id);
            db.Liquid_Filling_Category.Remove(liquid_Filling_Category);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
